# Brilliant take-home exercise

## Requirements

* `node` : `>=14`
* `npm`

# Setup

## Installation


```sh 
$ npm ci
```

## Build Assets

### One time build assets for development

```sh
$ npm run build
```

### Start a development server - reloading automatically after each file change.

```sh
$ npm run dev
```

# Production 

## Build Assets

Optimize assets for production by:

```sh
$ npm run production
```
