class Demo {
  constructor(d) {    
    this.vm = 1200
    this.scene = 600
    this.mirror = 200
    this.object = 50
    this.shape = 20
    this.circle = 20
    this.tStyle = "preserve-3d"  
    this.duration = .5
    this.template = document.querySelector('template')
  }
  init_top_view(){
    return new Promise((resolve, reject)=>{
      gsap.set('.top', {width:this.scene, height: this.scene, z:0, transformPerspective: this.vm, transformStyle: this.tStyle})
      gsap.set('.top .front', {x:200, y:315, width: this.mirror, height:this.mirror/40})
      gsap.set('.top .back', {x:200, y:520, width: this.mirror, height:this.mirror/40, opacity:0})
      gsap.set('.top .shape', {x:80, y:180, borderWidth: this.shape})
      gsap.set('.top .distance', {x:300, y:180, height: this.mirror, scaleY:0, transformOrigin:"100% 100%"})
      gsap.set('.top .circle.r0', {x:290, y:370, width:this.circle, height:this.circle})
      resolve()
    })
  }
  init_labels(){
    return new Promise((resolve, reject)=>{
      gsap.set('.top .front label', {x:205, y:-6})
      gsap.set('.top .back label', {x:205, y:-6})
      gsap.set('.top .r0 label', {x:25, y:1})
      gsap.set('.top .r1 label', {x:25, y:1})
      gsap.set('.top .r2 label', {x:25, y:1})
      resolve()
    })
  }
  init_controllers(){
    return new Promise((resolve, reject)=>{
      gsap.set('.top .triangle', {x:200, y:280, rotationZ:0, width: this.mirror, height:this.mirror})
      gsap.set('.top .dot', {x:200, y:280, width: this.mirror, height:this.mirror})
      resolve()
    })
  }
  init_perspective_view(){
    return new Promise((resolve, reject)=>{
      gsap.set('.bg', {width:this.scene, height: this.scene, top:26})
      gsap.set('.perspective', {width:this.scene, height: this.scene, z:-400, transformPerspective: this.vm, transformStyle: this.tStyle})
      gsap.set('.perspective .front', {x:200, y:200, z:0, width: this.mirror, height:this.mirror, transformPerspective: this.vm, transformStyle: this.tStyle})
      gsap.set('.perspective .circle', {x:278, y:278, z:50, width: this.circle, height:this.circle, transformPerspective: this.vm, transformStyle: this.tStyle})
      gsap.set('.perspective .c1', {x:298, y:298, z:-35, width: this.circle, height:this.circle, transformPerspective: this.vm, transformStyle: this.tStyle})
      gsap.set('.perspective .c2', {x:388, y:388, z:-400, width: this.circle, height:this.circle, transformPerspective: this.vm, transformStyle: this.tStyle})
      gsap.set('.perspective .back', {x:165, y:165, z:200, width: this.mirror, height:this.mirror, transformPerspective: this.vm, transformStyle: this.tStyle})
      resolve()
    })
  }
  line(pos){
    const div = this.template.content.cloneNode(true),
          lmax = (pos.type == 'ext')?document.querySelectorAll('.lights .l-ext').length:document.querySelectorAll('.lights .l').length,
          svg = div.querySelector('svg'),
          line = svg.querySelector('line')      
            
    line.setAttribute('x1',pos.x1)
    line.setAttribute('y1',pos.y1)
    line.setAttribute('x2',pos.x2)
    line.setAttribute('y2',pos.y2)
    line.setAttribute('stroke',pos.color)
    line.setAttribute('stroke-dasharray',pos['stroke-dasharray'])    
    
    if(pos.type == 'ext'){
      svg.classList.add(`l-ext`)
      svg.classList.add(`lx${lmax}`)
    }else{
      svg.classList.add(`l`)
      svg.classList.add(`l${lmax}`)
    }
    
    document.querySelector('.lights').appendChild(div)   
  }
  init_reflection_0(){
    return new Promise((resolve, reject)=>{
      gsap.set('.top .circle.r1', {x:290, y:245, width:this.circle, height:this.circle, opacity:0})      
      gsap.set('.top .circle.r2', {opacity:0})      
      gsap.set('.top .circle.choice0', {x:200, y:245, width:this.circle, height:this.circle, opacity:.7})      
      gsap.set('.top .circle.choice1', {x:290, y:245, width:this.circle, height:this.circle, opacity:.7})      
      gsap.set('.top .circle.choice2', {x:380, y:245, width:this.circle, height:this.circle, opacity:.7}) 
      gsap.set('.top .circle.choice3', {opacity:0})      
      gsap.set('.top .circle.choice4', {opacity:0})      
      gsap.set('.top .circle.choice5', {opacity:0})      
      gsap.set('.interactive', {opacity:0})
      gsap.set('.interactive.i0', {x:273, y:320, width:this.circle, height:this.circle})
      gsap.set('.interactive.i0 .slider', {x:-5, y:-12, width:this.circle, height:this.circle})
      gsap.set('.slider .angle-0', {x:25, y:10})
      gsap.set('.slider .angle-1', {x:-15, y:10})
      
      this.line({x1:295,y1:380,x2:273,y2:320, color:'yellow', "stroke-dasharray":0})
      this.line({x1:273,y1:320,x2:252,y2:380, color:'yellow', "stroke-dasharray":0})      
      this.line({x1:234,y1:430,x2:293,y2:265, color:'yellow', type:'ext', "stroke-dasharray":4})   
      
      gsap.set('.lights, .pview',{opacity:0})
      
      document.querySelectorAll('.choice0, .choice1, .choice2').forEach(c=>c.onclick = e => {
        if(c.classList.contains('choice1')){
          document.querySelector('mark').classList.remove('no')
          document.querySelector('mark').classList.add('yes')
          document.querySelector('mark').innerText = "That's correct!"
          document.querySelector('p').innerText = "Remember, in order to determine the position of the reflected image, we use a simple rule: the angles that the incoming and outgoing light make with the mirror's surface are equal (θ). Try moving the slider positionned at the intersection of the incoming and outgoing light to see different paths for the light."
          
          gsap.set('.top .circle.choice0', {opacity:0})      
          gsap.set('.top .circle.choice1', {opacity:0})      
          gsap.set('.top .circle.choice2', {opacity:0})  
          gsap.set('.top .circle.r1',{opacity:.8})
          gsap.set('.lights',{opacity:1})
          gsap.set('.interactive.i0', {opacity:1})
          gsap.set('.slider .angle-0', {opacity:1})
          gsap.set('.slider .angle-1', {opacity:1})
          
          this.show_reflection_0()
//          gsap.set('.interactive', {opacity:1})
//          gsap.set('.slider .angle-0', {opacity:1})
//          gsap.set('.slider .angle-1', {opacity:1})
          
        }else{
          document.querySelector('mark').classList.add('no')
          document.querySelector('mark').innerText = (document.querySelector('mark').innerText == 'Try again')?'Wrong':'Try again'
        }
      })
      
      document.querySelector('p').onclick = e=>{
        if(e.currentTarget.classList.contains('done')){
          this.init_reflection_1()
          this.end()
        }
      }

      Draggable.create('.interactive.i0', {lockAxis:true, type:'x', bounds:{minX: 240, maxX:350}, onDrag:function(){   
        gsap.set('svg.l0 line', {attr:{x2:this.x, y2:this.y}})
        gsap.set('svg.l1 line', {attr:{x1:this.x, y1:this.y, x2: this.x - (gsap.getProperty('svg.l0 line','x1').baseVal.value - this.x) }})  
        gsap.set('.interactive.i0', {x:this.x, y:this.y})
        
        /* Let's use some math here :)        
        line equation is y=mx+b
        so x = (y-b)/m
        but we also know m = (y1-y0)/(x1-x0)
        */
        
        const _b = (320-(380-320)/(254.75-274.875)*274.875),
              _x1 = gsap.getProperty('svg.l1 line','x1').baseVal.value,
              _y1 = gsap.getProperty('svg.l1 line','y1').baseVal.value,
              _x2 = gsap.getProperty('svg.l1 line','x2').baseVal.value,
              _y2 = gsap.getProperty('svg.l1 line','y2').baseVal.value,
              l_y1 = 435,
              l_x1 = (l_y1-(_y1-(_y2-_y1)/(_x2-_x1)*_x1))/((_y2-_y1)/(_x2-_x1)),
              l_y2 = 265,
              l_x2 = (l_y2-(_y1-(_y2-_y1)/(_x2-_x1)*_x1))/((_y2-_y1)/(_x2-_x1)),
              _m = (380-320)/(254.75-274.875)
                            
        gsap.set('svg.lx0 line', {attr:{x1:l_x1, y1:l_y1, x2:l_x2, y2:l_y2}})
//        console.log('l0',gsap.getProperty('svg.l0 line','x1').baseVal.value, gsap.getProperty('svg.l0 line','y1').baseVal.value, gsap.getProperty('svg.l0 line','x2').baseVal.value, gsap.getProperty('svg.l0 line','y2').baseVal.value)
//        console.log('l1',gsap.getProperty('svg.l1 line','x1').baseVal.value, gsap.getProperty('svg.l1 line','y1').baseVal.value, gsap.getProperty('svg.l1 line','x2').baseVal.value, gsap.getProperty('svg.l1 line','y2').baseVal.value)
//        console.log('lx0',gsap.getProperty('svg.lx0 line','x1').baseVal.value, gsap.getProperty('svg.lx0 line','y1').baseVal.value, gsap.getProperty('svg.lx0 line','x2').baseVal.value, gsap.getProperty('svg.lx0 line','y2').baseVal.value)    
        if(this.x>265 && this.x<282){
          document.querySelector('mark').classList.remove('no')
          document.querySelector('mark').classList.add('yes')
          document.querySelector('mark').innerText = 'The viewer can see the mirror image'
          document.querySelector('p').innerHTML = "At this angle, the light intersects with the viewer's line of sight. <a href=#>Now let's introduce our second mirror (click here).</a>"       
          document.querySelector('p').classList.add('done')
        }else{
          document.querySelector('mark').classList.remove('yes')
          document.querySelector('mark').classList.add('no')
          document.querySelector('mark').innerText = 'The viewer cannot see the mirror image'
          document.querySelector('p').innerText = "At this angle, the light doesn't intersect with the viewer's line of sight."
        }
      }})  
      resolve()
    })
  }
  show_reflection_0(s=0){
    return new Promise((resolve, reject)=>{
      let tl = gsap.timeline()      
      tl.to('.top .triangle', {rotationZ: 50, duration: this.duration})
      tl.to('.perspective', {rotationY: 40, duration: this.duration}, '<')    
      tl.from('svg.l0 line', {attr:{x2:gsap.getProperty('svg.l0 line','x1').baseVal.value,y2:gsap.getProperty('svg.l0 line','y1').baseVal.value}, duration: this.duration})  
      tl.from('svg.l1 line', {attr:{x2:gsap.getProperty('svg.l1 line','x1').baseVal.value,y2:gsap.getProperty('svg.l1 line','y1').baseVal.value}, duration: this.duration}) 
      tl.from('svg.lx0 line', {attr:{x2:gsap.getProperty('svg.lx0 line','x1').baseVal.value,y2:gsap.getProperty('svg.lx0 line','y1').baseVal.value}, duration: this.duration})  
      tl.seek(s);
      resolve()
    })
  }
  init_reflection_1(){
    return new Promise((resolve, reject)=>{
      gsap.set('.top .circle.r1', {opacity:0})
      gsap.set('.top .circle.r2', {x:290, y:70, width:this.circle, height:this.circle, opacity:0})
      gsap.set('.top .circle.choice3', {x:290, y:245, width:this.circle, height:this.circle, opacity:.7})      
      gsap.set('.top .circle.choice4', {x:290, y:70, width:this.circle, height:this.circle, opacity:.7})      
      gsap.set('.top .circle.choice5', {x:380, y:265, width:this.circle, height:this.circle, opacity:.7})   
      gsap.set('.interactive.i0', {opacity:0})
      gsap.set('.interactive.i1', {x:278, y:525, width:this.circle, height:this.circle, opacity:0})
      gsap.set('.interactive.i1 .slider', {x:-5, y:-12, width:this.circle, height:this.circle})
      gsap.set('.perspective .c2', {opacity:0})
      gsap.set('.perspective .c1', {opacity:0})
      gsap.set('.top .back', {x:200, y:520, width: this.mirror, height:this.mirror/40, opacity:1})
      
      document.querySelector('.lights').removeChild(document.querySelector('.l0'))
      document.querySelector('.lights').removeChild(document.querySelector('.l1'))
      document.querySelector('.lights').removeChild(document.querySelector('.lx0'))
      
      gsap.set('.lights, .pview',{opacity:0})
      gsap.set('.pview',{opacity:1})
      
      this.line({x1:295,y1:380,x2:278,y2:525, color:'yellow', "stroke-dasharray":0})
      this.line({x1:278,y1:525,x2:250,y2:320, color:'yellow', "stroke-dasharray":0})
      this.line({x1:250,y1:320,x2:230,y2:435, color:'yellow', "stroke-dasharray":0})
      this.line({x1:250,y1:320,x2:293,y2:90, color:'yellow', type:"ext", "stroke-dasharray":4})
      
      gsap.set('.slider .angle-0', {x:25, y:-15})
      gsap.set('.slider .angle-1', {x:-15, y:-15})
      
      document.querySelector('mark').classList.remove('yes')
      document.querySelector('mark').classList.remove('no')
      document.querySelector('mark').classList.remove('done')
      document.querySelector('mark').innerText = ""
      document.querySelector('p').innerHTML = "We now have a second mirror placed behind our object, and parallel to Mirror 1.<br/>Try to guess where the position of the Mirror 2 image will be, from the viewer points of view."
      
      document.querySelectorAll('.choice3, .choice4, .choice5').forEach(c=>c.onclick = e => {
        if(c.classList.contains('choice4')){
          document.querySelector('mark').classList.remove('no')
          document.querySelector('mark').classList.add('yes')
          document.querySelector('mark').innerText = "That's correct!"
          document.querySelector('p').innerHTML = "Since the angle of incidence (incoming light) always equals the angle of reflection (outgoing light): when lights bounce of Mirror 2, it will then bounce back from Mirror 1 before reaching the viewer's line of sight.<br/><br/>* Click on the mirror images from the Perspective view to see their corresponding light paths.<br/><br/>** Use the slider to change the angle of the light paths."
          
          gsap.set('.top .circle.choice3', {opacity:0})      
          gsap.set('.top .circle.choice4', {opacity:0})      
          gsap.set('.top .circle.choice5', {opacity:0})  
          gsap.set('.top .circle.r1',{opacity:.8})
          gsap.set('.top .circle.r2',{opacity:.8})
          gsap.set('.lights',{opacity:1})
          gsap.set('.interactive.i1', {opacity:1})
          gsap.set('.slider .angle-0', {opacity:1})
          gsap.set('.slider .angle-1', {opacity:1})          
          gsap.set('.perspective .c2', {opacity:1})
          gsap.set('.perspective .c1', {opacity:1})
          
          this.show_reflection_1()
          
          document.querySelector('.perspective .c2').classList.add('active')
          document.querySelector('.perspective .c1').classList.remove('active')
        }else{
          document.querySelector('mark').classList.add('no')
          document.querySelector('mark').innerText = (document.querySelector('mark').innerText == 'Try again')?'Wrong':'Try again'
        }
      })
      
      document.querySelector('.perspective .c2').onclick = e=>{
        if(!e.currentTarget.classList.contains('active')){
          e.currentTarget.classList.add('active')
          document.querySelector('.perspective .c1').classList.remove('active')
          this.init_reflection_1()
          document.querySelector('mark').classList.remove('no')
          document.querySelector('mark').classList.remove('yes')
          document.querySelector('mark').innerText = ""          
          gsap.set('.top .circle.choice3', {opacity:0})      
          gsap.set('.top .circle.choice4', {opacity:0})      
          gsap.set('.top .circle.choice5', {opacity:0})  
          gsap.set('.top .circle.r1',{opacity:.8})
          gsap.set('.top .circle.r2',{opacity:.8})
          gsap.set('.lights',{opacity:1})
          gsap.set('.interactive.i1', {opacity:1})
          gsap.set('.slider .angle-0', {opacity:1})
          gsap.set('.slider .angle-1', {opacity:1})          
          gsap.set('.perspective .c2', {opacity:1})
          gsap.set('.perspective .c1', {opacity:1})
        this.show_reflection_1()
        }

      }
      document.querySelector('.perspective .c1').onclick = e=>{
        if(!e.currentTarget.classList.contains('active')){
          e.currentTarget.classList.add('active')
          document.querySelector('.perspective .c2').classList.remove('active')
          document.querySelector('.lights').removeChild(document.querySelector('.l0'))
          document.querySelector('.lights').removeChild(document.querySelector('.l1'))
          document.querySelector('.lights').removeChild(document.querySelector('.l2'))
          document.querySelector('.lights').removeChild(document.querySelector('.lx0'))
          this.init_reflection_0()
          document.querySelector('mark').classList.remove('no')
          document.querySelector('mark').classList.remove('yes')
          document.querySelector('mark').innerText = ''
          gsap.set('.top .circle.choice0', {display:'none'})      
          gsap.set('.top .circle.choice1', {display:'none'})      
          gsap.set('.top .circle.choice2', {display:'none'})              
          gsap.set('.top .circle.r1',{opacity:.8})
          gsap.set('.top .circle.r2',{opacity:.8})
          gsap.set('.lights',{opacity:1})
          gsap.set('.interactive.i0', {opacity:1})
          gsap.set('.slider .angle-0', {opacity:1})
          gsap.set('.slider .angle-1', {opacity:1})   
          gsap.set('.pview',{opacity:1})
          this.show_reflection_0()
        }
      }
      
      
      Draggable.create('.interactive.i1', {lockAxis:true, type:'x', bounds:{minX: 250, maxX:340}, onDrag:function(){   
        gsap.set('svg.l0 line', {attr:{x2:this.x, y2:this.y}})
        gsap.set('svg.l1 line', {attr:{x1:this.x, y1:this.y, x2: 2*this.x - gsap.getProperty('svg.l0 line','x1').baseVal.value }})  
        gsap.set('svg.l2 line', {attr:{x1:gsap.getProperty('svg.l1 line','x2').baseVal.value, y1:gsap.getProperty('svg.l1 line','y2').baseVal.value, x2:this.x - 2*(gsap.getProperty('svg.l0 line','x1').baseVal.value - this.x) }})
        gsap.set('.interactive.i1', {x:this.x, y:this.y})
        const _b = (320-(380-320)/(254.75-274.875)*274.875),
              _x1 = gsap.getProperty('svg.l2 line','x1').baseVal.value,
              _y1 = gsap.getProperty('svg.l2 line','y1').baseVal.value,
              _x2 = gsap.getProperty('svg.l2 line','x2').baseVal.value,
              _y2 = gsap.getProperty('svg.l2 line','y2').baseVal.value,
              l_y1 = 420,
              l_x1 = (l_y1-(_y1-(_y2-_y1)/(_x2-_x1)*_x1))/((_y2-_y1)/(_x2-_x1)),
              l_y2 = 80,
              l_x2 = (l_y2-(_y1-(_y2-_y1)/(_x2-_x1)*_x1))/((_y2-_y1)/(_x2-_x1)),
              _m = (380-320)/(254.75-274.875)
        
        gsap.set('svg.lx0 line', {attr:{x1:l_x1, y1:l_y1, x2:l_x2, y2:l_y2}})
//        console.log('a',gsap.getProperty('svg.l1 line','x1').baseVal.value, gsap.getProperty('svg.l1 line','y1').baseVal.value)
//        console.log('b',gsap.getProperty('svg.l1 line','x2').baseVal.value, gsap.getProperty('svg.l1 line','y2').baseVal.value)             
        if(this.x>268 && this.x<282){
          document.querySelector('mark').classList.remove('no')
          document.querySelector('mark').classList.add('yes')
          document.querySelector('mark').innerText = 'The viewer can see the Mirror 2 image.'
          document.querySelector('p').innerHTML = "At this angle, the light intersects with the viewer's line of sight. By repeating the same process with the mirror images, this is how the infinite abyss mirror effect works."       
          document.querySelector('p').classList.add('done')
        }else{
          document.querySelector('mark').classList.remove('yes')
          document.querySelector('mark').classList.add('no')
          document.querySelector('mark').innerText = 'The viewer cannot see the Mirror 2 image'
          document.querySelector('p').innerText = "At this angle, the light doesn't intersect with the viewer's line of sight."
        }
      }})
      
      resolve()
    })
  }
  show_reflection_1(s=0){
    return new Promise((resolve, reject)=>{
      let tl = gsap.timeline()      
      tl.to('.top .triangle', {rotationZ: 50, duration: this.duration})     
      tl.to('.perspective', {rotationY: 40, duration: this.duration}, '<')
      tl.from('svg.l0 line', {attr:{x2:gsap.getProperty('svg.l0 line','x1').baseVal.value,y2:gsap.getProperty('svg.l0 line','y1').baseVal.value}, duration: this.duration})  
      tl.from('svg.l1 line', {attr:{x2:gsap.getProperty('svg.l1 line','x1').baseVal.value,y2:gsap.getProperty('svg.l1 line','y1').baseVal.value}, duration: this.duration})  
      tl.from('svg.l2 line', {attr:{x2:gsap.getProperty('svg.l2 line','x1').baseVal.value,y2:gsap.getProperty('svg.l2 line','y1').baseVal.value}, duration: this.duration})  
      tl.from('svg.lx0 line', {attr:{x2:gsap.getProperty('svg.lx0 line','x1').baseVal.value,y2:gsap.getProperty('svg.lx0 line','y1').baseVal.value}, duration: this.duration})  
      tl.seek(s);
      resolve()
    })
  }
  start(){
    return new Promise((resolve, reject)=>{
      let tl = gsap.timeline()      
      tl.to('.top .triangle', {rotationZ: 50, duration: this.duration})      
      document.querySelector('p').innerHTML = "Now that you understand how a mirror image is formed, let's see what happens when we introduce a second mirror.<br/><br/>But first, can you guess where the reflection of our red object will be shown from the viewer's point of view?"
      resolve()
    })
  }
  end(){
    return new Promise((resolve, reject)=>{
      let tl = gsap.timeline()      
      tl.from('.top .triangle', {rotationZ: 0, duration: this.duration})
      tl.from('.perspective', {rotationY: 0, duration: this.duration}, '<')
      resolve()
    })
  }
  init() {    
    gsap.registerPlugin(Draggable);
    this.init_top_view()
      .then(()=>this.init_labels())
      .then(()=>this.init_controllers())
      .then(()=>this.init_perspective_view())
      .then(()=>this.start())
      .then(()=>this.init_reflection_0())
//      .then(()=>this.show_reflection_0())
//      .then(()=>this.init_reflection_1())      
//      .then(()=>this.show_reflection_1())
  }
}

document.addEventListener('DOMContentLoaded', () =>(new Demo).init())